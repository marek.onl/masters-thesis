from sage.misc.flatten import flatten
from sage.misc.randstate import set_random_seed

from config import reload
from experiments import AES

set_random_seed()
reload('experiments', 'AES')


def test_1():
    aes = AES(n=2, r=1, c=2, e=4)
    polys = aes.ps_key_vars(n=2, method='substitution')

    keys = polys.guess_keys(polys=polys.reduce(),
                            guess=2,
                            w_exp=2,
                            method='magma')

    assert flatten(keys), 'No valid keys found.'
