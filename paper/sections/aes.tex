\section{\uppercase{Equation Systems for Small Scale AES}}\label{sec:aes}

This section describes the derivation of multivariate non-linear polynomial systems over GF(2) for small scale variants of AES.

% TENTO ODSEK BOL PRESUNUTY DO 1. KAPITOLY:

%The original name of the cipher for the Advanced Encryption Standard (AES) is
%Rijndael, based on the names of two cryptographers---Joan Daemen and Vincent
%Rijmen---who originally designed the cipher. In 1997, the U.S. National
%Institute of Standards and Technology (NIST) announced the development of AES
%and subsequently organized an open competition, which the Rijndael cipher won.
%NIST published the cipher as the Federal Information Processing Standard (FIPS)
%197 \cite{fips} in 2001.

\subsection{Small Scale AES}
\label{subsec:aes}

Before we discuss the scaled-down derivatives of AES, let us try to estimate
how long it would take to attack the full \mbox{AES-128} by brute force. The
actual time complexity of guessing a key with 128 bits can be illustrated by a
brief thought experiment.

Suppose we are in possession of a computer cluster with ten billion nodes, each
of which runs at \SI{3.3}{\giga\hertz}. Also suppose that one use of
\mbox{AES-128} takes only one clock cycle on each node. Say that one year has
around $3 \cdot 10^7$ seconds. Our cluster will then go through $3 \cdot 10^7
\cdot 3.3 \cdot 10^9 \cdot 10^{10} \approx 10^{27} \approx 2^{90}$ keys in one year.
This means that in the worst case, the total time required to guess the correct
key will be around $2^{38}$ years, which is about 250 billion; while the age of
the universe is currently estimated to be around 13.8 billion years.

Now suppose that the average consumption of each node is only \SI{1}{\watt} and
that \SI{1}{\kWh} of energy costs only \SI{0.01}{\eur} (the average price of
\SI{1}{\kWh} for European household consumers was around \SI{0.2}{\eur} in
2020). This means that the energy cost required for our attack is around $10^{10}
\cdot 0.001 \cdot 0.01 \cdot 24 \cdot 365 \cdot 2^{38} \approx 10^{20}
\text{ \euro}$.

A~quick estimate like this immediately leads to the conclusion that the
feasibility of the classic brute-force approach is beyond reality. This striking
infeasibility of attacking the full \mbox{AES-128} motivated researchers to come
up with scaled down versions of the cipher in order to provide manageable
insight into its internals. Carlos Cid et al. introduced such versions in
\cite{cid05} and \cite{cid06}. The reductions emerge naturally and the new
cipher can be described by the following parameters:
\begin{enumerate}[label=(\roman*), leftmargin=*,align=left]
\item the number of rounds $n$, $1 \le n \le 10$;
\item the number of rows $r$ of the state, $r = 1, 2, 4$;
\item the number of columns $c$ of the state, $c = 1, 2, 4$;
\item the number of bits $e$ of the elements of the state, $e = 4, 8$.
\end{enumerate}
We will denote the scaled-down version of AES by SR($n, r, c, e$). This
notation is consistent with \cite{cid05} and \cite{cid06}. The standard
\mbox{AES-128} can be then defined by SR($10, 4, 4, 8$) with one subtle
difference described in the following paragraph.

The last round of AES differs from the previous ones inasmuch as the
\verb=MixColumns= operation is omitted in it. This omission is due to the design
of the inverse of AES. The new SR($n, r, c, e$) cipher keeps the
\verb=MixColumns= operation in the last round. This operation is a linear
transformation, so the overall complexity of the cryptanalysis of both ciphers
remains the same, since a solution of a system of polynomial equations for one
cipher would provide a solution for the other cipher. This omission is the only
difference between \mbox{AES-128} and SR($10, 4, 4, 8$).

Let us now go through the scaled-down versions of the actual encryption
operations used in SR($n, r, c, e$). The cipher operates over the field
GF($2^e$), defined by the quotient ring $\mathbb{F}_2\!\left[ x \right] / \langle
f\!\left( x \right) \rangle$ where $f(x) = x^4 + x + 1$ when $e = 4$ and $f\!\left( x
\right) = x^8 + x^4 + x^3 + x + 1$ when $e = 8$. Note that the polynomial $f(x)$
is irreducible over $\mathbb{F}_2[x]$ in both cases and when $e = 8$, it is
identical to the polynomial used in the original \mbox{AES-128}.

The \verb=SubBytes= operation is also identical to the one used in
\mbox{AES-128} when $e = 8$. When $e = 4$, the operation is a composition of the
following two transformations:
\begin{enumerate}[label=(\roman*),leftmargin=*,align=left]
\item Take the multiplicative inverse in GF($2^4$), the element $0_{16}$ is mapped
  to itself.
\item Apply the following affine transformation over GF($2^4$):
  \begin{equation}  
    \label{eq:small_sbox}    
    \begin{pmatrix}
      b_0' \\
      b_1' \\
      b_2' \\
      b_3'
    \end{pmatrix}
    =
    \begin{pmatrix}
      1 & 0 & 1 & 1 \\
      1 & 1 & 0 & 1 \\
      1 & 1 & 1 & 0 \\
      0 & 1 & 1 & 1
    \end{pmatrix}
    \begin{pmatrix}
      b_0 \\
      b_1 \\
      b_2 \\
      b_3
    \end{pmatrix}
    +
    \begin{pmatrix}
      0 \\
      1 \\
      1 \\
      0
    \end{pmatrix}
    .
  \end{equation}
\end{enumerate}

The \verb=ShiftRows= operation cyclically rotates the row $i$ of the state by
$i$ positions, $0 \le i < r - 1$. Notice that we index the rows from zero so
that the first row is always left intact. When $r = 4$, we use the matrix
\begin{equation}
  \label{eq:R_4}
  R_4=
  \begin{pmatrix}
    0 & 1 & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1 \\
    1 & 0 & 0 & 0
  \end{pmatrix}
\end{equation}
to rotate a single row. When $r = 2$, the matrix becomes
\begin{equation*}
  \label{eq:R_2}
  R_2 =
  \begin{pmatrix}
    0 & 1 \\
    1 & 0
  \end{pmatrix}.
\end{equation*}
When $c = 4$, we can use the following expression to model the whole
\verb=ShiftRows= operation
\begin{equation}
  \label{eq:shiftrows}
  \begin{pmatrix}
    r_0' \\
    r_1' \\
    r_2' \\
    r_3'
  \end{pmatrix}
  =
  \begin{pmatrix}
    I & 0  & 0   & 0 \\
    0 & R_4 & 0   & 0 \\
    0 & 0  & R^2_4 & 0 \\
    0 & 0  & 0   & R^3_4
  \end{pmatrix}
  \begin{pmatrix}
    r_0 \\
    r_1 \\
    r_2 \\
    r_3
  \end{pmatrix}.
\end{equation}
We substitute $R_2$ instead of $R_4$ when $r = 2$. When $c = 2$, the expression
simply becomes
\begin{equation}
  \label{eq:small_shiftrows}
  \begin{pmatrix}
    r_0' \\
    r_1'
  \end{pmatrix}
  =
  \begin{pmatrix}
    I & 0 \\
    0 & R
  \end{pmatrix}
  \begin{pmatrix}
    r_0 \\
    r_1
  \end{pmatrix}
\end{equation}
where $R$ is either $R_4$ or $R_2$ and $I$ is the identity matrix of corresponding
size. When $r = 1$ or $c = 1$, the operation has no effect since either $R_2$
becomes $(1)$ or the matrix from the expression above becomes $I$.

The \verb=MixColumns= operation in AES multiplies each column of the state by
the polynomial
\[
  a\!\left( x \right) = 03_{16} x^3 + 01_{16} x^2 + 01_{16} x + 02_{16} \in
  \text{GF}\!( 2^8 )[x].
\]
Multiplication by a fixed polynomial modulo another fixed polynomial can be
regarded as a linear transformation so that the \verb=MixColumns= operation can
be seen as a linear transformation as well. If we associate the coefficients
$03_{16}$, $02_{16}$ and $01_{16}$ of the polynomial $a(x) \in
\text{GF}(2^8)[x]$ with the polynomials $x + 1$, $x$ and $1$, respectively, we
can model the \verb=MixColumns= operation by the following expression \begin{equation}
  \label{eq:mixcol}
  \begin{pmatrix}
    s_{0,c}' \\
    s_{1,c}' \\
    s_{2,c}' \\
    s_{3,c}'
  \end{pmatrix}
  =
  \begin{pmatrix}
      x   & x + 1 &   1   &   1   \\
      1   &   x   & x + 1 &   1   \\
      1   &   1   &   x   & x + 1 \\
    x + 1 &   1   &   1   &   x
  \end{pmatrix}
  \begin{pmatrix}
    s_{0,c} \\
    s_{1,c} \\
    s_{2,c} \\
    s_{3,c}
  \end{pmatrix}
\end{equation}
for $0 \le c < 4$, which indexes the columns. When $r = 2$, the operation can be
described by the following linear transformation
\begin{equation}
  \label{eq:small_mixcol}
  \begin{pmatrix}
    s_{0,j}' \\
    s_{1,j}'
  \end{pmatrix}
  =
  \begin{pmatrix}
     x + 1 &   x   \\
       x   & x + 1
  \end{pmatrix}
  \begin{pmatrix}
    s_{0,j} \\
    s_{1,j}
  \end{pmatrix}
\end{equation}
for $0 \le j < 2$, which indexes the columns, similarly to expression
\eqref{eq:mixcol}. When $r = 1$, the matrix defining the \verb=MixColumns=
operation simply becomes $(1)$, so the operation has no effect.

When $c = 4$, the new cipher uses the same key schedule as in \mbox{AES-128}.
For $c = 2$ and $c = 1$, the structure is naturally reduced and depicted in
Figure~\ref{fig:1_2_keyschedule}, left and right respectively. Similarly to
\mbox{AES-128}, the \verb=AddRoundKey= operation takes in $c$ words of length
$r$. Each word contains the elements of GF($2^e$). These elements are added to
the state---each word is added to a column of the state. The \verb=RotWord= and
\verb=SubWord= operations take in $r$-tuples containing the elements of
GF($2^e$). The round constant array also contains $r$-tuples, in which the only
non-zero element is the first one, namely $x^{j - 1} \in \text{GF}(2^e)$ being
the powers of $x \in \text{GF}(2^e)$ where $j$ is the round number. Notice that
the initial key has $rce$ bits. Also recall that this initial key is added to
the plaintext before starting the encryption and generating the subsequent
sub-keys, just as in \mbox{AES-128}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.5\columnwidth]{1_2_keyschedule}
  \cprotect\caption{A~schematic depiction of the scaled-down key schedule
    \cite{cid05}.}
  \label{fig:1_2_keyschedule}
\end{figure}

% The AES is an example of an iterated substitution-permutation network where
% one iteration is split into three stages \cite{shannon}. The first stage is a
% local nonlinear transformation (substitution) of the sub-blocks of the state.
% This transformation is performed by the \verb=SubBytes= operation --- the S-box
% is locally applied to each sub-block in order to substitute its value, while the
% mutual positions of the sub-blocks are left intact. This stage provides
% so-called confusion. The next stage is a global linear transformation of the
% state. This is performed by the \verb=ShiftRows= and \verb=MixColumns=
% operations, which are linear transformations over GF($2^e$), and which also
% change the mutual positions of the sub-blocks. This stage provides so-called
% diffusion and it tries to distribute the output bits of the S-boxes in the
% current iteration to as many S-box inputs as possible in the next iteration. The
% last stage is the addition of the key.

\subsection{Equations Systems}
\label{subsec:equations}

Let us now model AES and its scaled-down variants as a system of
multivariate polynomial equations over GF(2). We will focus our attention mainly
to SR($n$, 2, 2, 4) and derive a system of equations for this cipher. A~solution
to this system will provide us with the encryption key. Other scaled-down
derivatives can be modeled in the same way, including AES itself. Note that
we will use one ciphertext with its corresponding plaintext for our model. Our
method therefore comes under the known-plaintext type of cryptanalysis.

\subsubsection{Non-linear Equations}
\label{subsec:non-lin}

Let us start by considering the inversion part of the S-box. We know that $bc =
1$, where $b \in \text{GF}(2^e)$ is the input and $c \in \text{GF}(2^e)$ is the
output of the S-box. This equation holds unless $b = 0$, in which case we have
$b = c = 0$ and we will say that a 0-inversion has taken place. The probability
of a 0-inversion occurring is quite low, namely $\frac{1}{16}$ when $e = 4$
and $\frac{1}{256}$ when $e = 8$, so the probability of no 0-inversion occurring
is $1 - \frac{1}{16} = \frac{15}{16}$ and $1 - \frac{1}{256} = \frac{255}{256}$.
Notice, however, that these probabilities hold for a single application of the
S-box. In SR($n, 2, 2, 4$), there are four applications of the S-box during the
encryption in one round, so the probability of no 0-inversions occurring during
the encryption is $(\frac{15}{16})^{4n}$. There are also two applications of the
S-box during the key schedule in one round, so the probability of no
0-inversions occurring during the key schedule is $(\frac{15}{16})^{2n}$. We
presume statistical independence of the 0-inversions.

The actual occurrence of a 0-inversion either during the encryption or key
schedule is deterministically given by the choice of the plaintext and initial
key. If we happen to hit a 0-inversion during the generation of the ciphertext,
we can simply disregard the current combination of the plaintext and key, and
pick another combination. The issue, as we will see later on, is that one of the
equations that model the S-box would have to change, and from a cryptanalyst point of view, we would not know which one it would have to be since we do not know the key. For
this reason, we will assume that no 0-inversions have occurred for the given
plaintext/key combination when we start generating the equations.

We may regard both $b = \sum_{i = 0}^{3} b_i x^i$ and $c = \sum_{i = 0}^{3} c_i x^i$ as
polynomials in GF(2)[$x$]. The product $bc$ modulo the polynomial $m(x) = x^4 +
x + 1$ is $r(x) = r_3 x^3 + r_2 x^2 + r_1 x + r_0$ where
\begin{equation}
  \label{eq:r}
  \begin{aligned}
    r_0 &= b_0 c_0 \oplus b_3 c_1 \oplus b_2 c_2 \oplus b_1 c_3, \\
    r_1 &= b_1 c_0 \oplus b_0 c_1 \oplus b_3 c_2 \oplus b_2 c_3 \oplus b_3 c_1 \oplus b_2 c_2 \oplus b_1 c_3, \\
    r_2 &= b_2 c_0 \oplus b_1 c_1 \oplus b_0 c_2 \oplus b_3 c_3 \oplus b_3 c_2 \oplus b_2 c_3, \\
    r_3 &= b_3 c_0 \oplus b_2 c_1 \oplus b_1 c_2 \oplus b_0 c_3 \oplus b_3 c_3.
  \end{aligned}
\end{equation}
It is important to note that the coefficients $b_i$ and $c_i$ are the elements of
GF($2$). We have $bc = r = 1$. This gives us four multivariate quadratic
equations over GF($2$): $r_0 = 1$ and $r_i = 0$ where $i = 1, 2, 3$. These
equations are bilinear in the variables $b_i$ and $c_i$. For $e = 8$, we would
have got eight multivariate quadratic equations in the variables $b_i$ and $c_i$
instead of four.

If there was a 0-inversion, either during the encryption or key schedule, the
first equation would change to $r_0 = 0$. However as already mentioned, we do
not consider this case, since we can detect 0-inversions before we start
generating the equations and disregard the plaintext/key combinations that
produce them.

Along with these equations, it is possible to obtain further quadratic equations
from the relation $bc = 1$. Notice that we also have $bc^2 = c$ and $b^2c = b$.
Let us focus on the first relation and compute the resulting equations. The
equations for $b^2c = b$ can be produced in the same fashion. Since we work over
GF(2), we can write $bc^2 + c = 0$. We have already computed the product $bc$,
so we could just multiply it by $c$ and get the result. This computation would
require unnecessary steps as it would lead to many intermediate cubic terms
which we would have to cross out before obtaining the final coefficients. We can
instead compute the square of $c$ and pre-multiply it by $b$. We are working
over a commutative structure, so the order in which we perform the
multiplication is of no relevance. In order to work out the square of $c$, we
can use \eqref{eq:r} and substitute $c$ for $b$. We get the polynomial $d = c^2$
where $d(x) = d_3 x^3 + d_2 x^2 + d_1 x + d_0$ with
\begin{align*}
d_0 &= c_0 \oplus c_2 & d_1 &=    c_2 & d_2 &= c_1 \oplus c_3 & d_3 &=    c_3.
\end{align*}
%\begin{align*}
%  d_0 &= c_0 \oplus c_2 \\
%  d_1 &=    c_2   \\
%  d_2 &= c_1 \oplus c_3 \\
%  d_3 &=    c_3.
%\end{align*}
We can now obtain the final result $t = bd + c$ where $t(x) = t_3 x^3 + t_2 x^2
+ t_1 x + t_0$ with
\begin{align*}
  t_0 &= b_0 c_0 \oplus b_0 c_2 \oplus b_3 c_2 \oplus b_2 c_1 \oplus b_2 c_3 \oplus b_1 c_3 \oplus c_1, \\
  t_1 &= b_1 c_0 \oplus b_1 c_2 \oplus b_0 c_2 \oplus b_3 c_1 \oplus b_3 c_3 \oplus b_3 c_2 \oplus b_2 c_1 \\
     &\quad \quad \quad  \oplus b_1 c_3 \oplus c_1, \\
  t_2 &= b_2 c_0 \oplus b_2 c_2 \oplus b_1 c_2 \oplus b_0 c_1 \oplus b_0 c_3 \oplus b_3c_1 \oplus b_2 c_3 \\
     &\quad \quad \quad  \oplus c_2, \\
  t_3 &= b_3 c_0 \oplus b_3 c_2 \oplus b_2 c_2 \oplus b_1 c_1 \oplus b_1 c_3 \oplus b_0 c_3 \oplus b_3 c_3 \\
     &\quad \quad \quad  \oplus c_3.
\end{align*}
We know that $t = 0$, so we have four equations $t_i = 0$ for $0 \le i < 4$.
Notice that these equations are quadratic as well. We can obtain reciprocal
equations from $b^2c = b$. All of these eight equations are biaffine in the $b_i$
and $c_i$ variables.

It is possible to obtain even more quadratic equations by considering the
relations $bc^4 = c^3$ and $b^4c = b^3$. As in the previous case, let us focus
on the first relation. We can square $d$ to obtain $c^4$ and multiply $d$ by $c$
to obtain $c^3$. The result will then be $u = bc^4 + c^3 = 0$ where $u(x) = u_3
x^3 + u_2 x^2 + u_1 x + u_0$ with
\begin{align*}
  u_0 &= b_3 c_3 \oplus b_3 c_1 \oplus b_2 c_3 \oplus b_2 c_2 \oplus b_1 c_3 \oplus b_0 c_3 \oplus b_0 c_2 \\
     &\quad \quad \quad  \oplus b_0 c_1 \oplus b_0 c_0 \oplus c_3 c_1 \oplus c_2 c_1 \oplus c_2 c_0 \oplus c_0, \\
  u_1 &= b_3 c_2 \oplus b_3 c_1 \oplus b_2 c_2 \oplus b_1 c_2 \oplus b_1 c_1 \oplus b_1 c_0 \oplus b_0 c_3 \\
     &\quad \quad \quad  \oplus b_0 c_1 \oplus c_3 c_2 \oplus c_2 c_0 \oplus c_1 c_0 \oplus c_3, \\
  u_2 &= b_3 c_2 \oplus b_2 c_2 \oplus b_2 c_1 \oplus b_2 c_0 \oplus b_1 c_3 \oplus b_1 c_1 \oplus b_0 c_3 \\
     &\quad \quad \quad  \oplus b_0 c_2 \oplus c_3 c_2 \oplus c_3 c_1 \oplus c_3 c_0 \oplus c_2 c_1 \oplus c_2 c_0 \\
     &\quad \quad \quad  \oplus c_1 c_0 \oplus c_2, \\
  u_3 &= b_3 c_2 \oplus b_3 c_1 \oplus b_3 c_0 \oplus b_2 c_3 \oplus b_2 c_1 \oplus b_1 c_3 \oplus b_1 c_2 \\
     &\quad \quad \quad  \oplus b_0 c_3 \oplus c_3 c_2 \oplus c_3 c_2 \oplus c_3 c_1 \oplus   c_3  \oplus c_2 \oplus c_1.
\end{align*}
We have another four equations $u_i = 0$ for $0 \le i < 4$. Observe that these
equations are still quadratic. We can obtain reciprocal equations from $b^4c =
b^3$.

% It is possible to obtain even more quadratic equations by considering the
% relations $bc^4 = c^3$ and $b^4c = b^3$. As in the previous case, let us focus our
% attention to the first one and rewrite it to $bc^4 + c^3 = 0$. We can square $d$
% to obtain $c^4$, so let $f = d^2$ where $f(x) = f_3 x^3 + f_2 x^2 + f_1 x + f_0$ with
% \begin{align*}
%   f_0 &= c_0 \oplus c_1 \oplus c_2 \oplus c_3 \\
%   f_1 &=      c_1 \oplus c_3 \\
%   f_2 &=      c_2 \oplus c_3 \\
%   f_3 &=         c_3.
% \end{align*}
% The polynomial $c^3$ can be obtained by multiplying $d$ by $c$. We then get
% $g = dc$ where $g(x) = g_3 x^3 + g_2 x^2 + g_1 x + g_0$ with
% \begin{align*}
%   g_0 &= c_0 \oplus c_0 c_2 \oplus c_1 c_2 \oplus c_2 c_3 \\
%   g_1 &= c_3 \oplus c_0 c_1 \oplus c_0 c_2 \oplus c_2 c_3 \\
%   g_2 &= c_2 \oplus c_0 c_1 \oplus c_0 c_2 \oplus c_0 c_3 \oplus c_1 c_2 \oplus c_1 c_3 \oplus c_2 c_3 \\
%   g_3 &= c_1 \oplus   c_2  \oplus   c_3  \oplus c_1 c_3 \oplus c_2 c_3.
% \end{align*}
% We are now in a position to obtain the result $u = bf + g$ where $u(x) = u_3 x^3
% + u_2 x^2 + u_1 x + u_0$ with
% \begin{align*}
%   u_0 &= b_3 c_3 \oplus b_3 c_1 \oplus b_2 c_3 \oplus b_2 c_2 \oplus b_1 c_3 \oplus b_0 c_3 \oplus b_0 c_2 \\
%      &\quad \quad \quad  \oplus b_0 c_1 \oplus b_0 c_0 \oplus c_3 c_1 \oplus c_2 c_1 \oplus c_2 c_0 \oplus c_0, \\
%   u_1 &= b_3 c_2 \oplus b_3 c_1 \oplus b_2 c_2 \oplus b_1 c_2 \oplus b_1 c_1 \oplus b_1 c_0 \oplus b_0 c_3 \\
%      &\quad \quad \quad  \oplus b_0 c_1 \oplus c_3 c_2 \oplus c_2 c_0 \oplus c_1 c_0 \oplus c_3, \\
%   u_2 &= b_3 c_2 \oplus b_2 c_2 \oplus b_2 c_1 \oplus b_2 c_0 \oplus b_1 c_3 \oplus b_1 c_1 \oplus b_0 c_3 \\
%      &\quad \quad \quad  \oplus b_0 c_2 \oplus c_3 c_2 \oplus c_3 c_1 \oplus c_3 c_0 \oplus c_2 c_1 \oplus c_2 c_0 \\
%      &\quad \quad \quad  \oplus c_1 c_0 \oplus c_2, \\
%   u_3 &= b_3 c_2 \oplus b_3 c_1 \oplus b_3 c_0 \oplus b_2 c_3 \oplus b_2 c_1 \oplus b_1 c_3 \oplus b_1 c_2 \\
%      &\quad \quad \quad  \oplus b_0 c_3 \oplus c_3 c_2 \oplus c_3 c_2 \oplus c_3 c_1 \oplus   c_3  \oplus c_2 \oplus c_1.
% \end{align*}
% We know that $u = 0$, so we have another four equations $u_i = 0$ for $0 \le i <
% 4$. Observe that these equations are still quadratic. We can obtain reciprocal
% equations from $b^4c = b^3$.

% We can obtain the same equations by following a different path presented in
% \cite[p. 76]{cid06}. Let us rewrite the expression \eqref{eq:r} into matrix
% form:
% \begin{equation}
%   \label{eq:mul}
%   \begin{pmatrix}
%     r_0 \\
%     r_1 \\
%     r_2 \\
%     r_3
%   \end{pmatrix}
%   =
%   \begin{pmatrix}
%        b_0    &    b_3    &   b_2    &    b_1   \\
%        b_1    & b_0 \oplus b_3 & b_3 \oplus b_2 & b_2 \oplus b_1 \\
%        b_2    &    b_1    & b_0 \oplus b_3 & b_3 \oplus b_2 \\
%        b_3    &    b_2    &   b_1    & b_0 \oplus b_3
%   \end{pmatrix}
%   \begin{pmatrix}
%     c_0 \\
%     c_1 \\
%     c_2 \\
%     c_3
%   \end{pmatrix}.
% \end{equation}
% If we substitute the polynomial $p(x) = x$ for $b$, we get the matrix
% \begin{equation*}
%   \label{eq:pmul}
%   T_x =
%   \begin{pmatrix}
%     0 & 0 & 0 & 1 \\
%     1 & 0 & 0 & 1 \\
%     0 & 1 & 0 & 0 \\
%     0 & 0 & 1 & 0
%   \end{pmatrix},
% \end{equation*}
% which represents the multiplication by the element $x \in \text{GF}(2^4)$. We
% can obtain a matrix which represents the multiplication by any element $b \in
% \text{GF}(2^4)$ by setting
% \begin{equation*}
%   \label{}
%   C_x = \left( T^3_x b~\middle|~T^2_x b~\middle|~T_x b~\middle|~b\right)
% \end{equation*}

So far, we have derived 20 multivariate quadratic equations from the relation
$bc = 1$. A~natural question arises whether we have identified all quadratic
equations in the $b_i$ and $c_i$ variables. Notice, for example, that we have
skipped the relation $bc^3 = c^2$. The reason is that it would produce equations
with cubic terms. Relations involving higher powers than $c^4$ would also lead
to equations with higher than quadratic terms. In fact, the 20 equations we have
derived are all the quadratic equations over GF(2). A~further discussion can be
found in \cite[p. 77]{cid06}. As also advised in \cite[p. 77]{cid06}, we will
focus on the first 12 bilinear and biaffine quadratic equations we have obtained
and we will omit the remaining eight ones. For $e = 8$, we would have got 40
multivariate quadratic equations in the variables $b_i$ and $c_i$ instead of 20.

\subsubsection{Linear Equations}
\label{subsec:lin}

The equations we have derived for the inversion part of the S-box account for
the only non-linear equations in the whole system that models the \mbox{SR($n$,
  2, 2, 4)} cipher. In fact, the inversion in the AES S-box represents the only
non-linear operation in the whole cipher. Let us now derive the linear equations
for the remaining transformations in AES.

The affine transformation of the S-box can be expressed directly by
\eqref{eq:small_sbox}, where the input is the polynomial $c(x)$ from the
previous subsection. This gives us four linear equations in the variables $c_i$.
These equations together with the non-linear equations from the previous
subsection fully describe a single S-box. Let $L_s$ denote the matrix from
\eqref{eq:small_sbox}. In order to describe the whole \verb=SubBytes= operation,
we can extend the matrix $L_s$ to the whole state array of SR($n$, 2, 2, 4), so
we have the matrix
\begin{equation*}
  L =
  \begin{pmatrix}
    L_s & 0 & 0 & 0 \\
    0 & L_s & 0 & 0 \\
    0 & 0 & L_s & 0 \\
    0 & 0 & 0 & L_s
  \end{pmatrix}.
\end{equation*}
We can also extend the S-box constant vector $(0, 1, 1, 0)^T = 6_{16}$ to the
vector $\mathbf{6} = (6_{16}, 6_{16}, 6_{16}, 6_{16})$ so that we cover the
whole state array. We will use $\mathbf{b}$ to denote the input vector of the
\verb=SubBytes= operation, and $\mathbf{b^{-1}}$ to denote its output---the
vector of the inverted elements in GF($2^4$). Note that each component in these
vectors is made of the four coefficients of the polynomials $b(x)$ and $c(x)$,
respectively; so we have 12 non-linear equations for each component.

\begin{figure}[h]
  \centering \includegraphics[width=.2\columnwidth]{small_state}
  \cprotect\caption{The state array of the SR($n$, 2, 2, $e$) cipher.}
  \label{fig:small_state}
\end{figure}

The actual state array is depicted in Figure \ref{fig:small_state}. We will
represent it as the vector $(s_0, s_1, s_2, s_3)^T$. The \verb=ShiftRows= operation
can be then described by the matrix
\begin{equation*}
  R =
  \begin{pmatrix}
    I_4 & 0 & 0 & 0 \\
    0 & 0 & 0 & I_4 \\
    0 & 0 & I_4 & 0 \\
    0 & I_4 & 0 & 0
  \end{pmatrix}
\end{equation*}
where $I_4$ is the identity matrix of size four. Before we describe the
\verb=MixColumns= operation, let us rewrite \eqref{eq:r} into matrix form:
\begin{equation*}
  \label{eq:mul}
  \begin{pmatrix}
    r_0 \\
    r_1 \\
    r_2 \\
    r_3
  \end{pmatrix}
  =
  \begin{pmatrix}
    b_0    &    b_3    &   b_2    &    b_1   \\
    b_1    & b_0 \oplus b_3 & b_3 \oplus b_2 & b_2 \oplus b_1 \\
    b_2    &    b_1    & b_0 \oplus b_3 & b_3 \oplus b_2 \\
    b_3    &    b_2    &   b_1    & b_0 \oplus b_3
  \end{pmatrix}
  \begin{pmatrix}
    c_0 \\
    c_1 \\
    c_2 \\
    c_3
  \end{pmatrix}.
\end{equation*}
If we substitute the binary values of the coefficients of the polynomials $x +
1$ and $x$ into the matrix in the expression above, we get the matrices
\begin{equation}
\begin{split}
  M_{x+1} & =
  \begin{pmatrix}
    1 & 0 & 0 & 1 \\
    1 & 1 & 0 & 1 \\
    0 & 1 & 1 & 0 \\
    0 & 0 & 1 & 1
  \end{pmatrix}
  \quad\text{and} \\
  M_x    & =
  \begin{pmatrix}
    0 & 0 & 0 & 1 \\
    1 & 0 & 0 & 1 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 1 & 0
  \end{pmatrix}.
\end{split}
\end{equation}
These matrices represent the multiplication by the polynomials $x + 1$ and $x$
modulo the polynomial $x^4 + x + 1$. The \verb=MixColumns= operation, defined by
\eqref{eq:small_mixcol}, can then be expressed by the matrix
\begin{equation*}
  M =
  \begin{pmatrix}
    M_{x+1} &   M_x  &   0   &   0   \\
      M_x  & M_{x+1} &   0   &   0   \\
      0   &   0   & M_{x+1} &  M_x   \\
      0   &   0   &  M_x   & M_{x+1}
  \end{pmatrix}.
\end{equation*}
We can now describe one round of SR($n$, 2, 2, 4) by the expression
\[
  \mathbf{b_i} = MR(L \mathbf{b_{i-1}^{-1}} + \mathbf{6}) + \mathbf{k_i} \quad
  \text{for} \quad i < 0 \le n
\]
where $\mathbf{k_i}$ is a vector containing 16 binary variables of the round key
described in the following subsection and $i$ is the round number. The vector
$\mathbf{b_{i-1}^{-1}}$ contains four components---the outputs from the
S-boxes---each of which has four binary variables. It is straightforward to
check that $R \mathbf{6} = M \mathbf{6} = \mathbf{6}$. We can then write
\[
  \mathbf{b_i} = MRL \mathbf{b_{i-1}^{-1}} + \mathbf{k_i} + \mathbf{6} \quad
  \text{for} \quad i < 0 \le n.
\]
The relation above gives 16 linear equations, which represent one round of
SR($n$, 2, 2, 4). In addition, we have 12 non-linear equations for each
component in $\mathbf{b_{i-1}^{-1}}$, so in total, we have $16 + 4 \cdot 12 =
64$ equations describing one round of encryption in the SR($n$, 2, 2, 4) cipher.
When $i = n$, we have
\[
  \mathbf{c_t} = MRL \mathbf{b_{n-1}^{-1}} + \mathbf{k_n} + \mathbf{6}
\]
where $\mathbf{c_t}$ is the known ciphertext, which is a vector of 16 binary
values. We obtain $\mathbf{b_0}$ by adding the initial unknown key
$\mathbf{k_0}$ to the known plaintext $\mathbf{p_t}$, so we have
\[
  \mathbf{b_0} = \mathbf{p_t} + \mathbf{k_0}.
\]
This addition gives us further 16 initial equations where $\mathbf{p_t}$ is a
vector of 16 binary values and $\mathbf{k_0}$ is a vector of 16 binary
variables. Our goal is to actually compute the values of $\mathbf{k_0}$ since
this is the user's~key. All other variables are auxiliary.

\subsubsection{Key Schedule}
\label{subsec:keyschedule}

The generation of round keys for SR($n, r, c, e$) is thoroughly described in
Appendix A of \cite{cid05}. Let us now describe the equations for SR($n$, 2, 2,
4). Let $\mathbf{k}_{\mathbf{i}} = \left( k_{i,0}, k_{i,1}, k_{i,2}, k_{i,3},
\right)^T \in GF(2^4)^4$ be the round key of round $i$. The round key can be then
defined by
\begin{equation}
\begin{split}
  \begin{pmatrix}
    k_{i,2q} \\
    k_{i,2q+1}
  \end{pmatrix}
  & =
  \begin{pmatrix}
    L k_{i-1,3}^{-1} \\
    L k_{i-1,2}^{-1}
  \end{pmatrix}
  +
  \begin{pmatrix}
    6_{16} \\
    6_{16}
  \end{pmatrix}
  +
  \begin{pmatrix}
    x^{i-1} \\
    0
  \end{pmatrix} \\
  & +
  \sum_{t=0}^{q}
  \begin{pmatrix}
    k_{i-1,2t} \\
    k_{i-1,2t+1}
  \end{pmatrix}
\end{split}
\end{equation}
for $0 \le q < 2$ where $x^{i-1}$ is an element of GF($2^4$). This expression
gives 16 linear equations for each $\mathbf{k_i}$. Note that $\mathbf{k_0}$ is
not provided by the user---it is a vector of 16 binary variables that we, as
the cryptanalyst, are trying to compute. We also get $2 \cdot 12 = 24$
non-linear equations since the computation of each $\mathbf{k_i}$ requires two
applications of the S-box. One round of the key schedule in SR($n$, 2, 2, 4) is
then described by 40 equations.

\subsubsection{Equations Without Auxiliary Variables}
\label{subsec:key_eqs}

In this section, we propose the derivation of equations that contain only the variables of the initial key.
In order to obtain such a system, we can eliminate the auxiliary variables by a
gradual substitution of the variables of the initial key since we know that the
cipher starts by adding the initial key to the known plaintext. It is
straightforward to perform this substitution for the linear equations. For the
non-linear equations, which model the S-box, we can leverage Gröbner bases.
Consider the four polynomials $r_0, \ldots, r_3$ from \eqref{eq:r} as
polynomials in $\F \! \left[ c_0, \ldots, c_3, b_0, \ldots, b_3 \right]$. We see
that it is not straightforward to express the output bits $c_i$ in terms of the
input bits $b_i$ by ordinary manipulation techniques. If we impose the graded
reverse lexicographic block order $\succeq_{grlex, grlex}$ on $\F \! \left[ c_0,
  \ldots, c_3, b_0, \ldots, b_3 \right]$ with $\succeq_{grlex}$ on both $\F \!
\left[ c_0, \ldots, c_3 \right]$ and $\F \! \left[ b_0, \ldots, b_3 \right]$,
and compute the reduced Gröbner basis, we get the following polynomial system:
\begin{align*}
  f_1 &= c0 \oplus b_2 b_1 b_0 \oplus b_3 b_2 b_1 \oplus b_2 b_0 \oplus b_2 b_1 \oplus b_0 \oplus b_1    \\
     & \qquad \  \oplus b_2 \oplus b_3,                                         \\
  f_2 &= c1 \oplus b_3 b_1 b_0 \oplus b_1 b_0 \oplus b_2 b_0 \oplus b_2 b_1 \oplus b_3 b_1 \oplus b_3,   \\
  f_3 &= c2 \oplus b_3 b_2 b_0 \oplus b_1 b_0 \oplus b_2 b_0 \oplus b_3 b_0 \oplus b_2 \oplus b_3,      \\
  f_4 &= c3 \oplus b_3 b_2 b_1 \oplus b_3 b_0 \oplus b_3 b_1 \oplus b_3 b_2 \oplus b_1 \oplus b_2       \\
     & \qquad \  \oplus b_3,                                               \\
  f_5 &= b_3 b_2 b_1 b_0 \oplus b_2 b_1 b_0 \oplus b_3 b_1 b_0 \oplus b_3 b_2 b_0 \oplus b_3 b_2 b_1 \\
     & \quad \quad \quad \quad \quad\! \oplus b_1 b_0 \oplus b_2 b_0 \oplus b_2 b_1 \oplus b_3 b_0 \oplus b_3 b_1    \\
     & \quad \quad \quad \quad \quad\! \oplus b_3 b_2 \oplus b_0 \oplus b_1 \oplus b_2 \oplus b_3 \oplus 1.
\end{align*}
We see that the last polynomial $f_5$ involves only the variables $b_i$. Notice
that this polynomial is not satisfied only if all $b_i = 0$, and it holds
whenever we have at least one $b_i = 1$. Recall that we do not consider
0-inversions. This polynomial is therefore always satisfied, and we can omit it
from the system. We also see that in the remaining polynomials, the output
variables $c_0, \ldots, c_3$ are expressed solely by the input variables $b_i$.
This allows us to perform the gradual substitution of the unknown variables of
the initial key $\mathbf{k_0}$ throughout the whole polynomial system. Notice
that we obtain $\abs{\mathbf{k_0}} = 16$ polynomials after we finish the
substitution. We note that the size of the polynomials is close to
$2^{\abs{\mathbf{k_0}} - 1}$ at full diffusion of the cipher. The diffusion grows
rapidly with each round. For example, as our experiments will reveal, the cipher
SR($n$, 2, 2, 4) reaches its full diffusion at round $n = 3$. This way of
generating the polynomials is therefore suitable only for low values of $n$.
A~different method for obtaining polynomials without auxiliary variables is
described in \cite{bulygin}.

% Yet another way of obtaining polynomials involving only the variables of the
% initial key $\mathbf{k_0}$ is to regard the cipher as a set of boolean functions
% of $\mathbf{k_0}$, one function per one bit of $\mathbf{k_0}$. We can then convert
% such functions into an Algebraic Normal Form (ANF) in order to obtain the
% polynomials. Such a conversion can be found in \cite{anf}. However, the
% complexity of this approach requires at least $\abs{\mathbf{k_0}}
% 2^{\abs{\mathbf{k_0}}}$ encryptions even if we consider only one round of the
% cipher. For this reason, we will not examine this method any further.
