\subsection*{MixColumns}
\label{subsec:mixcols}

This transformation operates on the individual columns of the state and is
depicted in Figure \ref{fig:mixcolumns}. Each column is considered as a
four-term polynomial, in which the coefficients are the four bytes constituting
the column. These bytes are considered as elements of GF($2^8$). With this
set-up, each column is multiplied modulo $x^4 + 1$ with the polynomial
\[
  a\!\left( x \right) = 03_{16} x^3 + 01_{16} x^2 + 01_{16} x + 02_{16} \in
  \text{GF}\!( 2^8 )[x].
\]
The multiplication is performed as in Example \ref{e:quotient}.

The coefficients were chosen so that \verb=MixColumns= is fast on 8-bit
architectures. The constant $01_{16}$ requires no processing at all, polynomial
multiplication by $02_{16}$ can be implemented by a shift and a conditional XOR.
Multiplication by $03_{16}$ can be implemented as multiplication by $02_{16}$
and an additional XOR.

Note that the polynomial $x^4 + 1$ is not irreducible over GF($2^8$)[$x$]
since $x^4 + 1 = \left( x^2 + 1 \right) \left( x^2 + 1 \right)$. This means that
the quotient ring $\text{GF($2^8$)}\!\left[ x \right] / \langle x^4 + 1 \rangle$
is not a finite field, which means that not each element has its multiplicative
inverse so that multiplication by a fixed polynomial is not necessarily
invertible. However, the polynomial $a\!\left( x \right)$ has its multiplicative
inverse, namely
\[
  a^{-1}\!\left( x \right) = 0\text{B}_{16} x^3 + 0\text{D}_{16} x^2 + 09_{16} x +
  0\text{E}_{16},
\]
so that multiplication by this polynomial is invertible and therefore
the\\\verb=MixColumns= operations remains invertible as well.

\begin{figure}[h]
  \centering \includegraphics[width=.8\textwidth]{mixcolumns}
  \cprotect\caption{The \verb=MicColumns= operation \cite{fips}.}
  \label{fig:mixcolumns}
\end{figure}

\FloatBarrier

\begin{re}
  \label{re:mul}
  Let $b(x) = b_3 x^3 + b_2 x^2 + b_1 x + b_0$ and $c(x) = c_3 x^3 + c_2 x^2 + c_1 x + c_0$
  be two polynomials in GF$(2^8)[x]$. Addition of these two polynomials consists
  of adding the coefficients of like powers of $x$. These coefficients are the
  elements of GF$(2^8)$, so addition of the coefficients effectively corresponds
  to their XOR and will be denoted by $\oplus$:
  \[
    b(x) + c(x) = \left( b_3 \oplus c_3 \right) x^3 + \left( b_2 \oplus c_2 \right)
    x^2 + \left( b_1 \oplus c_1 \right) x + \left( b_0 \oplus c_0 \right).
  \]
  As in Example \ref{e:quotient}, multiplication modulo the polynomial $m(x) =
  x^4 + 1$ is performed in two stages. At first, we obtain the full product $b(x)
  c(x) = d(x)$ where
  \[
    d(x) = d_6 x^6 + d_5 x^5 + d_4 x^4 + d_3 x^3 + d_2 x^2 + d_1 x + d_0
  \]
  with
  \begin{align*}
    d_0 &= b_0 c_0, \\
    d_1 &= b_1 c_0 \oplus b_0 c_1, \\
    d_2 &= b_2 c_0 \oplus b_1 c_1 \oplus b_0 c_2, \\
    d_3 &= b_3 c_0 \oplus b_2 c_1 \oplus b_1 c_2 \oplus b_0 c_3, \\
    d_4 &= b_3 c_1 \oplus b_2 c_2 \oplus b_1 c_3, \\
    d_5 &= b_3 c_2 \oplus b_2 c_3, \\
    d_6 &= b_3 c_3.
  \end{align*}
  Now we divide $d(x)$ by $m(x)$ and obtain the quotient $q(x)$ and remainder
  $r(x)$:
  \[
    d(x) = q(x) m(x) + r(x)
  \]
  where $q(x) = d_6 x^2 + d_5 x + d_4$ and $r(x) = r_3 x^3 + r_2 x^2 + r_1 x + r_0$ with
  \begin{align*}
    r_0 &= b_0 c_0 \oplus b_3 c_1 \oplus b_2 c_2 \oplus b_1 c_3, \\
    r_1 &= b_1 c_0 \oplus b_0 c_1 \oplus b_3 c_2 \oplus b_2 c_3, \\
    r_2 &= b_2 c_0 \oplus b_1 c_1 \oplus b_0 c_2 \oplus b_3 c_3, \\
    r_3 &= b_3 c_0 \oplus b_2 c_1 \oplus b_1 c_2 \oplus b_0 c_3.
  \end{align*}
  Note that we may express the coefficients $r_i$ in matrix notation:
  \begin{equation}
    \begin{pmatrix}
      r_0 \\
      r_1 \\
      r_2 \\
      r_3
    \end{pmatrix}
    =
    \begin{pmatrix}
      b_0 & b_3 & b_2 & b_1 \\
      b_1 & b_0 & b_3 & b_2 \\
      b_2 & b_1 & b_0 & b_3 \\
      b_3 & b_2 & b_1 & b_0
    \end{pmatrix}
    \begin{pmatrix}
      c_0 \\
      c_1 \\
      c_2 \\
      c_3
    \end{pmatrix}
    .\label{eq:multiplication}
  \end{equation}
  We take the remainder $r(x)$, as the actual result of the overall
  multiplication of $b(x)$ and $c(x)$, so we can write
  \[
    b(x) c(x) \equiv r(x) \pmod{m(x)}.
  \]
  Observe that we consider $b(x)$ as a fixed polynomial and use its coefficients
  to fill the matrix in expression \eqref{eq:multiplication}.
\end{re}

The remark above shows that multiplication by a fixed polynomial modulo another
fixed polynomial can be regarded as a linear transformation, so that the
\verb=MixColumns= operation can be seen as a linear transformation as well. As
shown in Remark \ref{re:hex}, the coefficients $03_{16}$, $02_{16}$ and $01_{16}$
of the polynomial $a(x) \in \text{GF}(2^8)[x]$ can be written as the polynomials
$x + 1$, $x$ and $1$, respectively. If we associate $a(x)$ with the polynomial
$b(x)$ from the remark above, we get the following expression
\begin{equation}
  \label{eq:mixcol}
  \begin{pmatrix}
    s_{0,c}' \\
    s_{1,c}' \\
    s_{2,c}' \\
    s_{3,c}'
  \end{pmatrix}
  =
  \begin{pmatrix}
      x   & x + 1 &   1   &   1   \\
      1   &   x   & x + 1 &   1   \\
      1   &   1   &   x   & x + 1 \\
    x + 1 &   1   &   1   &   x
  \end{pmatrix}
  \begin{pmatrix}
    s_{0,c} \\
    s_{1,c} \\
    s_{2,c} \\
    s_{3,c}
  \end{pmatrix}
\end{equation}
for $0 \le c < 4$, which indexes the columns. This expression fully describes
the \verb=MixColumns= operation.
