\section{Small Scale Variants of AES}
\label{sec:small}

Before we define the scaled-down derivatives of AES, let us try to estimate
how long it would take to attack the full \mbox{AES-128} by brute force. The
actual time complexity of guessing a key with 128 bits can be illustrated by a
brief thought experiment.

Suppose we are in possession of a computer cluster with ten billion nodes, each
of which runs at \SI{3.3}{\giga\hertz}. Also suppose that one use of the
\mbox{AES-128} takes only one clock cycle on each node. Say that one year has
around $3 \cdot 10^7$ seconds. Our cluster will then go through $3 \cdot 10^7 \cdot 3.3 \cdot
10^9 \cdot 10^{10} \approx 10^{27} \approx 2^{90}$ keys in one year. This means that in the
worst case, the total time required to guess the correct key will be around
$2^{38}$ years, which is about 250 billion; while the age of the universe is
currently estimated to be around 13.8 billion years.

Now suppose that the average consumption of each node is only \SI{1}{\watt} and
that \SI{1}{\kWh} of energy costs only \SI{0.01}{\eur} (the average price of
\SI{1}{\kWh} for European household consumers is around \SI{0.2}{\eur} in 2020).
This means that the energy cost required for our attack is around $10^{10} \cdot
0.001 \cdot 0.01 \cdot 24 \cdot 365 \cdot 2^{38} \approx$ \SI{e20}{\eur}.

A~quick estimate like this immediately leads to the conclusion that the
feasibility of the classic brute-force approach is beyond reality. This striking
infeasibility of attacking the full \mbox{AES-128} motivated researchers to come
up with scaled down versions of the cipher in order to provide manageable
insight into its internals. Carlos Cid et al. introduced such versions in
\cite{cid05} and \cite{cid06}. The reductions emerge naturally and the new
cipher can be described by the following parameters:
\begin{enumerate}[label=(\roman*)]
\item the number of rounds $n$, $1 \le n \le 10$;
\item the number of rows $r$ of the state, $r = 1, 2, 4$;
\item the number of columns $c$ of the state, $c = 1, 2, 4$;
\item the number of bits $e$ of the elements of the state, $e = 4, 8$.
\end{enumerate}
We will denote the scaled-down version of AES by SR($n, r, c, e$). This
notation is consistent with \cite{cid05} and \cite{cid06}. The standard
\mbox{AES-128} can be then defined by SR($10, 4, 4, 8$) with one subtle
difference described in the following paragraph:

As shown in Algorithm \ref{alg:aes}, the last round differs from the previous
ones inasmuch as the \verb=MixColumns= operation is omitted in it. This omission
is due to the design of the inverse of AES. The new SR($n, r, c, e$) cipher
keeps the \verb=MixColumns= operation in the last round. In section
\ref{sec:aes}, we saw that this operation is a linear transformation, so the
overall complexity of the cryptanalysis of both ciphers remains the same, since
a solution of a system of polynomial equations for one cipher would provide a
solution for the other cipher. This omission is the only difference between the
\mbox{AES-128} and SR($10, 4, 4, 8$).

Let us now go through the scaled-down versions of the actual encryption
operations used in SR($n, r, c, e$). The cipher operates over the field
GF($2^e$), defined by the quotient ring $\mathbb{F}_2\!\left[ x \right] / \langle
f\!\left( x \right) \rangle$ where $f(x) = x^4 + x + 1$ when $e = 4$ and $f\!\left( x
\right) = x^8 + x^4 + x^3 + x + 1$ when $e = 8$. Note that the polynomial $f(x)$
is irreducible over $\mathbb{F}_2[x]$ in both cases and when $e = 8$, it is
identical to the polynomial used in the original \mbox{AES-128}.

The \verb=SubBytes= operation is also identical to the one used in the
\mbox{AES-128} when $e = 8$. When $e = 4$, the operation is a composition of the
following two transformations:
\begin{enumerate}[label=(\roman*)]
\item Take the multiplicative inverse in GF($2^4$), the element $0_{16}$ is mapped
  to itself.
\item Similarly to expression \eqref{eq:sbox}, apply the following affine
  transformation over GF($2^4$):
  \begin{equation}
    \label{eq:small_sbox}
    \begin{pmatrix}
      b_0' \\
      b_1' \\
      b_2' \\
      b_3'
    \end{pmatrix}
    =
    \begin{pmatrix}
      1 & 0 & 1 & 1 \\
      1 & 1 & 0 & 1 \\
      1 & 1 & 1 & 0 \\
      0 & 1 & 1 & 1
    \end{pmatrix}
    \begin{pmatrix}
      b_0 \\
      b_1 \\
      b_2 \\
      b_3
    \end{pmatrix}
    +
    \begin{pmatrix}
      0 \\
      1 \\
      1 \\
      0
    \end{pmatrix}
    .
  \end{equation}
\end{enumerate}

The \verb=ShiftRows= operation cyclically rotates the row $i$ of the state by
$i$ positions, $0 \le i < r - 1$. Notice that we index the rows from zero so
that the first row is always left intact. When $r = 4$, we can use the matrix
$R_4$ from \eqref{eq:R_4}. When $r = 2$, the matrix to rotate a row becomes
\begin{equation*}
  \label{eq:R_2}
  R_2 =
  \begin{pmatrix}
    0 & 1 \\
    1 & 0
  \end{pmatrix}.
\end{equation*}
When $c = 4$, we can use the matrix from expression \eqref{eq:shiftrows} and
alternatively use $R_2$ instead of $R_4$ when $r = 2$. When $c = 2$, the
expression simply becomes
\begin{equation}
  \label{eq:small_shiftrows}
  \begin{pmatrix}
    r_0' \\
    r_1'
  \end{pmatrix}
  =
  \begin{pmatrix}
    I & 0 \\
    0 & R
  \end{pmatrix}
  \begin{pmatrix}
    r_0 \\
    r_1
  \end{pmatrix}
\end{equation}
where $R$ is either $R_4$ or $R_2$ and $I$ is the identity matrix of corresponding
size. When $r = 1$ or $c = 1$, the operation has no effect since either $R_2$
becomes $(1)$ or the matrix from the expression above becomes $I$.

The \verb=MixColumns= operation remains the same as in \mbox{AES-128} when
$r = 4$. When $r = 2$, the operation is defined by the following linear
transformation:
\begin{equation}
  \label{eq:small_mixcol}
  \begin{pmatrix}
    s_{0,j}' \\
    s_{1,j}'
  \end{pmatrix}
  =
  \begin{pmatrix}
     x + 1 &   x   \\
       x   & x + 1
  \end{pmatrix}
  \begin{pmatrix}
    s_{0,j} \\
    s_{1,j}
  \end{pmatrix}
\end{equation}
for $0 \le j < 2$, which indexes the columns, similarly to expression
\eqref{eq:mixcol}. When $r = 1$, the matrix defining the \verb=MixColumns=
operation simply becomes $(1)$, so the operation has no effect.

When $c = 4$, the new cipher uses the same key schedule as in the
\mbox{AES-128}. For $c = 2$ and $c = 1$, the structure is naturally reduced and
depicted in Figure \ref{fig:1_2_keyschedule} left and right respectively.
Similarly to \mbox{AES-128}, the \verb=AddRoundKey= operation takes in $c$
words of length $r$. Each word contains the elements of GF($2^e$). These
elements are added to the state --- each word is added to a column of the state,
as shown in formula \eqref{eq:addRoundKey}. The \verb=RotWord= and \verb=SubWord=
operations take in $r$-tuples containing the elements of GF($2^e$). The round
constant array also contains $r$-tuples, in which the only non-zero element is
the first one, namely $x^{j - 1} \in \text{GF}(2^e)$ being the powers of $x \in
\text{GF}(2^e)$ where $j$ is the round number. Notice that the initial key has
$rce$ bits. Also recall that this initial key is added to the plaintext before
starting the encryption and generating the subsequent sub-keys, just as in the
\mbox{AES-128}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.5\textwidth]{1_2_keyschedule}
  \cprotect\caption{A~schematic depiction of the scaled-down key schedule
    \cite{cid05}.}
  \label{fig:1_2_keyschedule}
\end{figure}