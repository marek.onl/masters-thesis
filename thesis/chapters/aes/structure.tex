\section{The Structure of AES}
\label{sec:aes}

Let us now walk through the structure of AES, which is thoroughly described
in \cite{fips}. Further information can be found in \cite{rijndael}, a~book
written by the original authors of Rijndael.

The AES is a symmetric block cipher. The block size is 128 bits. There are three
possible key sizes: 128, 192 and 256. The original Rijndael cipher is more
liberal as regards the block and keys sizes, as these values can be
independently set to any multiple of 32 bits within the range from 128 to 256
bits. This scale is the only difference between Rijndael and AES. We will
focus our attention solely to the version with both the block and key size
set to 128 bits, which is referred to as AES-128. This restriction will have
almost no impact on generality since the structure of all the ciphers remains
the same up to certain constants. We will also define an even more granular and
principally smaller structure of the cipher in the following section.

The input as well as the output of the cipher can be considered as a
one-dimensional array of 16 bytes. The cipher operates on a two-dimensional $4
\times 4$ array of 16 bytes called the \textbf{state}.

\begin{re}
  Bytes in the state can be considered as polynomials of the form
  \[
    \sum_{i=0}^{7} b_i x^i,
  \]
  where $b_i \in \mathbb{F}_2$ are the individual bits, see Remark \ref{re:ff}.
  These polynomials are the elements of the quotient ring $\mathbb{F}_2\!\left[
    x \right] / \langle f\!\left( x \right) \rangle$, where $f\!\left( x \right) = x^8 + x^4
  + x^3 + x + 1 \in \mathbb{F}_2\!\left[ x \right]$ is an irreducible polynomial
  over $\mathbb{F}_2\!\left[ x \right]$, so the quotient ring
  $\mathbb{F}_2\!\left[ x \right] / \langle f\!\left( x \right) \rangle$ is in fact a finite
  field --- as described in Example \ref{e:quotient} and Proposition
  \ref{pr:quotient}. We will also denote this finite field by GF($2^8$).
\end{re}

\begin{re} \label{re:hex}
  Another way to describe a byte is by its hexadecimal value, e.g. $63_{16}$
  represents $01100011_2$, or as described in the previous remark, $03_{16}$
  represents the polynomial $x + 1$. Also note that a byte can be regarded as a
  vector in an 8-dimensional vector space over GF(2). We will employ all these
  views on bytes throughout this chapter. Moreover, a~word consisting of four
  bytes can also be regarded as a vector in an 4-dimensional vector space over
  GF($2^8$).
\end{re}

The overall structure of the cipher is described in Algorithm \ref{alg:aes}. At
the beginning, the initial key is expanded into 44 bytes, which are then used
throughout the encryption. This expansion is described in Algorithm
\ref{alg:aes-key} and Section \ref{sec:key}. The plaintext is then copied
into the state and the initial key addition is performed. The cipher then
performs a cycle with nine iterations. We call these iterations \textbf{rounds}.
The lines 10 and 11 comprise the tenth round, with the exception of the
\verb=MixColumns= operation being omitted. This omission is due to the design of
the inverse cipher --- it makes the structure of the inverse cipher more
consistent with the structure described in Algorithm \ref{alg:aes}. The inverse
cipher is thoroughly described in \cite{fips}. Let us remark that all the
operations that manipulate the state have their inverted counterparts and the
inverse cipher can be implemented by applying these inverted operations in
reverse order where the key schedule is reversed as well.

Let us now go over the individual operations described in the following
sections. Note that a prime on a variable (e.g. $c'$) denotes the
updated value of the variable. Also note that each byte in the state has two
indices: the row index $0 \le r < 4$ and the column index $0 \le c < 4$, so that
a specific byte in the state can be denoted $s_{r,c}$.

\begin{algorithm}[h]
  \caption{High Level Overview of AES}\label{alg:aes}

  \begin{tabular}{ l l }
    \textbf{Input:}  & $\bm{\mathit{plaintext}}$ --- array of 16 bytes, \\
            & $\bm{\mathit{key}}$ --- array of 16 bytes \\
    \textbf{Output:} & ciphertext $\bm{\mathit{state}}$ --- array of 16 bytes \\
  \end{tabular}
  \\
  \begin{algorithmic}[1]
    \Function{AES}{$\mathit{plaintext}$, $\mathit{key}$}
    \State $\mathit{expKey} \gets$ ExpandKey($\mathit{key}$)
    \State $\mathit{state} \gets \mathit{plaintext}$
    \State $\mathit{state} \gets$ AddRoundKey($\mathit{state}$, $\mathit{expKey}[0 : 3]$)

    \For{$\mathit{round} \gets 1$ to 9}
    \State $\mathit{state} \gets$ MixColumns(ShiftRows(SubBytes($\mathit{state}$)))
    \State $\mathit{state} \gets$ AddRoundKey($\mathit{state}$,
    $\mathit{expKey}[4 \cdot \mathit{round} : 4 \cdot (\mathit{round} + 1) - 1]$)
    \EndFor

    \State $\mathit{state} \gets$ ShiftRows(SubBytes($\mathit{state}$))
    \State $\mathit{state} \gets$ AddRoundKey($\mathit{state}$, $\mathit{expKey}[40 : 43]$)

    \State \Return $state$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\FloatBarrier