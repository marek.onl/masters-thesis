\section{Algorithms for Computing Gröbner Bases}
\label{sec:algs}

This section discusses an educational version of the F4 algorithm for computing
Gröbner bases. Such a version is described in \cite[Chapter 10, \S3]{iva} which
we will follow. The algorithm was introduced in \cite{f4} by Jean-Charles
Faugère in 1999. It was tailored for the degree reverse lexicographic order. The
author recommends computing a Gröbner basis via F4 using this order and then
convert it to the lexicographic order by another algorithm, for example the FGLM
algorithm \cite{fglm}, in order to apply the Elimination Theorem afterwards. We
will also state the Buchberger's~criterion which is one of the main claims about
Gröbner bases.

\begin{df}
  Let $f, g \neq 0 \in \Fg$ be polynomials. If $\mathrm{multideg}(f) = \alpha$
  and $\mathrm{multideg}(g) = \beta$, then let $\gamma = \sn{\gamma}$, where
  $\gamma_i = \mathrm{max}(\alpha_i, \beta_i)$ for each $i$. We call $x^\gamma$ the
  \textbf{least common multiple} of $\mathrm{LM}(f)$ and $\mathrm{LM}(g)$, and
  we write $x^\gamma = \mathrm{lcm}(\mathrm{LM}(f), \mathrm{LM}(g))$. The
  \textbf{S-polynomial} of $f$ and $g$ is the combination
  \[
    S(f, g) = \frac{x^\gamma}{\mathrm{LT}(f)} \cdot f -
    \frac{x^\gamma}{\mathrm{LT}(g)} \cdot g.
  \]
\end{df}

\begin{e}
  Let $f = x^2y^3 + x$ and $g = 2xy^5 + y$ be polynomials in $\R[x,y]$ under
  $\succeq_{grlex}$. The least common multiple is $x^2y^5$ and
  \begin{align*}
    S(f, g) &= \frac{x^2y^5}{x^2y^3} \left( x^2y^3 + x \right) - \frac{x^2y^5}{2xy^5}
    \left( 2xy^5 + y \right) \\
            &= x^2y^5 + xy^2 - x^2y^5 - \frac{1}{2} xy \\
            &= xy^2 - \frac{1}{2} xy.
  \end{align*}
  Observe that the S-polynomial is designed so that the leading terms of $f$ and
  $g$ cancel each other out. If both $f$ and $g$ consisted only of the leading
  terms, $S(f, g)$ would be 0.
\end{e}

\begin{thm}[Buchberger's Criterion]
  A basis $G = \set*{\s[m]{g}}$ of an ideal $I$ is a Gröbner basis if and only
  if $\overline{S(g_i, g_j)}^G = 0$ for all $i \neq j$.
\end{thm}

\begin{p}
  See \cite[p. 86]{iva}.
\end{p}

The theorem above, introduced by Bruno Buchberger in \cite{bb}, is one of the
key results about Gröbner bases. It allows us to test whether a given basis is a
Gröbner basis in polynomial time. It also naturally leads to an algorithm,
called the Buchberger's~algorithm, which constructs a Gröbner basis for an ideal
by adding nonzero remainders $\overline{S(g_i, g_j)}^G$ to $G$ until the
Buchberger's~criterion eventually holds.

\begin{df} \sloppy Let $\succeq$ be a monomial order on $\M(\mb{x})$ and let $G
  = \set*{\s[m]{g}} \subseteq \Fg$ be a set of polynomials. For any $f \in \Fg$,
  we say that $f$ \textbf{reduces to zero modulo} $G$, and write $f \to_G 0$, if
  $f$ has a \textbf{standard representation}
  \[
    f = \sum_{i=1}^{m} h_ig_i, \  h_i \in \Fg,
  \]
  which means that whenever $h_ig_i \neq 0$, we have $\LM{f} \succeq \LM{h_ig_i}$.
\end{df}

We note that $\overline{f}^G = 0$ implies $f \to_G 0$, but the converse does not hold.

\begin{pr}
  \label{pr:gb_crit}
  A basis $G = \set*{\s[m]{g}}$ of an ideal $I$ is a Gröbner basis if and only
  if $S(g_i, g_j) \to_G 0$ for all $i \neq j$.
\end{pr}

\begin{p}
  This is a more general version of the Buchberger's criterion. The proof can be
  found in \cite[p. 105]{iva}.
\end{p}

Le us now describe Algorithm~\ref{alg:f4}. The value of $m$ records the
cardinality of $G$ throughout the whole run of the algorithm. The set $B$
represents an unordered list of pairs of polynomials for which the corresponding
S-polynomials are not known to reduce to zero. Observe that we add further pairs
into $B$ on line 17. The algorithm ends when all pairs in $B$ have been
processed. In our case, the while loop starts by selecting all pairs with the
minimal degree in the set $B$. The degree of a pair $\set*{i, j}$ is defined as
\[
  \deg{\set*{i, j}} = \dg{\lcm{\LM{f_i},  \LM{f_j}}} \in \N.
\]
This selection is called the \textit{normal selection strategy} and it is
recommended by Faugère. Other strategies are possible too though. The value $d$
is used only for selecting the pairs with the minimal degree on line 7 and
nowhere else. We note that $d$ is the same as the total degree of the leading
monomial of both halves
\begin{equation}
  \label{eq:halves}
  \dfrac{\lcm{\LM{f_i},\;\LM{f_j}}}{\LT{f_i}} \cdot f_i \quad \text{and} \quad
  \dfrac{\lcm{\LM{f_i},\;\LM{f_j}}}{\LT{f_j}} \cdot f_j
\end{equation}
of the S-polynomial $S(f_i, f_j)$. This value might not be the same as the total
degree of the S--polynomial itself --- it can be smaller or greater or the same,
depending on the other terms in $f_i$ and $f_j$ and on the monomial order. 

The Buchberger's algorithm would now compute the remainders $\overline{S(f_i,
  f_j)}^G$ for $\{i, j\} \in B'$ and add them to $G$ so that the Buchberger's
criterion eventually holds. The F4 algorithm uses a generalized version of the
Buchberger's criterion stated in Proposition~\ref{pr:gb_crit} and computes
$\overline{S(f_i, f_j)}$ defined by the equation
\begin{equation}
  \label{eq:spoly}
  \overline{S(f_i, f_j)} = S(f_i, f_j) - c_1x^{\alpha(1)}f_{k_1} - c_2x^{\alpha(2)}f_{k_2}
  - \cdots.
\end{equation}
When $\overline{S(f_i, f_j)}$ is included in $G$, we get a standard
representation of $S(f_i, f_j)$ so that Proposition~\ref{pr:gb_crit} holds.

Let us now show how $\overline{S(f_i, f_j)}$ is obtained. We start by creating
the set $L$ which contains the two polynomials from \eqref{eq:halves} for each
pair $\set*{i, j} \in B'$. Since $\set*{i, j}$ is unordered, both are included.
Recall that the difference of the two polynomials for the pair $\set*{i, j}$
gives $S(f_i, f_j)$. The sets $L$ and $G$ are then passed to the
$\textrm{SymbolicPreprocessing}$ function. This function returns the matrix $M$
representing a set of polynomials as described in the following remark.

\begin{re}
  A~tuple $F$ of polynomials can be represented by a matrix $A$ and a vector $v$
  in the following way. Consider the set $M_\succeq(F) = \set*{m_n, \ldots, m_0}$ of all
  monomials in $F$ under the monomial order $\succeq$. Let $v = \left( m_n, \ldots, m_0
  \right)^T$ be the vector containing all the monomials in $F$ ordered in
  decreasing order, and let $A[i, j]$ contain the coefficient of the monomial
  $m_j \in M_\succeq(F)$ in $f_i \in F$. The rows of the product $A v$ then represent the
  original polynomials in $F$. We can think of $v$ as being implicit and
  consider only the matrix $A$. The rows of $A$ then represent the polynomials
  in $F$ as well.
\end{re}

Let us now discuss the function $\textrm{SymbolicPreprocessing}$ that creates
the matrix $M$. The function is described in Algorithm~\ref{alg:sp}. We see that
$M$ actually represents the set $H$. This set has the following two properties:
\begin{enumerate}[label=(\roman*)]
\item $L \subseteq H$, and
\item whenever $x^\beta$ is a monomial in some $f \in H$ and there exists some $f_k \in
  G$ with $\LM{f_k} \mid x^\beta$, then $H$ contains a product $x^\alpha f_k$ with $\LM{x^\alpha
    f_k} = x^\beta$.
\end{enumerate}

The algorithm starts by including all polynomials from $L$ into $H$. This
satisfies property (i). The algorithm then continues until all monomials in $H$
are processed. The processed monomials are put into the set $\mathit{done}$. The
monomials in both $\mathit{done}$ and $M_\succeq(H)$ are ordered in decreasing order
according to $\succeq$. Note that when $x^\beta$ is equal to the leading monomial of one
of the polynomials in $L$, property (ii) is satisfied, so we put all the leading
monomials in $H$ into $\mathit{done}$ before we start the while loop. The
algorithm then repeatedly selects the largest monomial $x^\beta \in M_\succeq(H)$. If there
exists some $f_k \in G$ such that $\LM{f_k} \mid x^\beta$, then we include
$\frac{x^\beta}{\LM{f_k}} f_k$ into $H$ so that property (ii) is satisfied for
$x^\beta$. When there are no further monomials to consider, the algorithm returns
the matrix $M$ corresponding to $H$.

The next step in Algorithm~\ref{alg:f4} is to compute the row reduced echelon
form of $M$. It can be shown that this computation gives the equations of the
form \eqref{eq:spoly}. The reason for including the products $x^\alpha f_k$ into $M$
was that the monomials $x^\beta$ cannot appear on the left-hand side
of~\eqref{eq:spoly} and must be canceled by something on the right-hand side. On
line 12, we pick those polynomials (rows) whose leading terms are not divisible
by the leading terms of any of the polynomials in $M$. These new polynomials
correspond to the $\overline{S(f_i, f_j)}$ in~\eqref{eq:spoly} and are included
in $G$ so that Proposition~\ref{pr:gb_crit} eventually holds.

For a better intuitive insight, consider again the matrix $M$ which represents
the set $H$. Recall that $L \subseteq H$ so $M$ contains both halves of $S(f_i, f_j)$
for any $\set*{i, j}$ that is currently in $B'$. The S-polynomial $S(f_i, f_j)$
is the difference of the two halves so it can be represented as a linear
combination of the rows of $M$. The rows of $N$ form a basis for the vector
space spanned by the rows of $M$. This means that $S(f_i, f_j)$ can be also
represented by a linear combination of the rows of $N$. It can be shown that
this gives $S(f_i, f_j) \to_G 0$ when we include the new polynomials from $N$ into
$G$ as described at the end of the previous paragraph.

\begin{algorithm}
  \caption{F4 Algorithm}\label{alg:f4}

  \begin{tabular}{l l}
    \textbf{Input:}  & a tuple of polynomials $\bm{ F = \left( f_1, \ldots, f_m \right)}$, \\
    \textbf{Output:} & a Gröbner basis \textbf{G} of $\bm{I = \langle f_1, \ldots, f_m \rangle}$ \\
  \end{tabular}
  \\
  \begin{algorithmic}[1]
    \Function{F4}{$F$}
    \State $G \gets F$
    \State $m \gets \abs{F}$
    \State $B \gets \{\{i, j\} \mid 1 \le i < j \le m\}$

    \While{$B \neq \emptyset$}
    \State $d \gets \Min{\dg{\set*{i, j}} \in \N \mid \set*{i, j} \in B}$
    \State $B' \gets \set*{\set*{i, j} \in B \sdel \dg{\set*{i, j}} = d}$
    \State $B \gets B~\backslash~B'$
    \State $L \gets \set*{\dfrac{\lcm{\LM{f_i},\;\LM{f_j}}}{\LT{f_i}} \cdot f_i \sdel \set*{i, j} \in B'}$
    \State $M \gets \textrm{SymbolicPreprocessing}(L, \, G)$
    \State $N \gets \text{row reduced echelon form of } M$
    \State $N^+ \gets \set*{n \in \mathrm{rows}(N) \sdel \LM{n} \notin \langle \LM{\mathrm{rows}(M)} \rangle}$

    \For{$n \in N^+$}
    \State $m \gets m + 1$
    \State $f_m = \text{polynomial form of } n$
    \State $G \gets G \cup \{f_m\}$
    \State $B \gets B \cup \set*{\set*{i, m} \sdel 1 \le i < m}$
    \EndFor
    \EndWhile

    \State \Return $G$
    \EndFunction
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}
  \caption{Symbolic Preprocessing}\label{alg:sp}

  \begin{tabular}{l l}
    \textbf{Input:}  & a set of polynomials $\bm{L}$, \\
                     & a set of polynomials $\bm{G}$, \\
    \textbf{Output:} & a matrix $\textbf{M}$ \\
  \end{tabular}
  \\
  \begin{algorithmic}[1]
    \Function{SymbolicPreprocessing}{$L,\ G$}
    \State $H \gets L$
    \State $\mathit{done} \gets \LM{H}$

    \While{$\mathit{done} \neq M_\succeq(H)$}
    \State $x^\beta \gets \text{the largest monomial in } \left( M_\succeq(H) \ \backslash\  \mathit{done} \right)$
    \State $\mathit{done} \gets \mathit{done} \cup \{x^\beta\}$
    \If{there exists $f_k \in G$ such that $\LM{f_k} \mid x^\beta$}
    \State select any one $f_k \in G$ such that $\LM{f_k} \mid x^\beta$
    \State $H \gets H \cup \set*{\frac{x^\beta}{\LM{f_k}}f_k}$
    \EndIf
    \EndWhile

    \State \Return matrix of coefficients of $H$ with respect to $M_\succeq(H)$ with \\
    \hspace{4.7em}       columns in decreasing order according to $\succeq$
    \EndFunction
  \end{algorithmic}
\end{algorithm}
