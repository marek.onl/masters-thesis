\section{Gröbner Bases and Systems of Equations}
\label{sec:gb}

Let us now define Gröbner bases and show that they can be used for solving
systems of equations.

\begin{df}
  Let $I \subseteq \Fg$ be an ideal different from $\{0\}$. We denote by
  $\mathrm{LT}(I) = \{\mathrm{LT}(f) \mid f \in I\}$ the set of leading terms of
  nonzero elements of $I$. The \textbf{ideal of leading terms} of $I$, generated
  by $\mathrm{LT}(I)$, will be denoted by $\langle \mathrm{LT}(I) \rangle$.
\end{df}

\begin{df}
  Fix a monomial order on $\Mx$. A~finite basis $G \subseteq I$ of a nonzero ideal $I \subseteq
  \Fg$ is a \textbf{Gröbner basis} if
  \[
    \langle\LT{G} \rangle = \langle \LT{I} \rangle.
  \] 
\end{df}

The definition above says that a set $\{\s[m]{g}\} \subseteq I$ is a Gröbner
basis if and only if the leading term of any element of $I$ is divisible by some
of the $\mathrm{LT}(g_i)$.

\begin{pr}
  Fix a monomial order on $\Mx$. Then every ideal $I \subseteq \Fg$ has a Gröbner basis.
\end{pr}

\begin{p}
  It can be shown that the proof can be seen as a corollary of Theorem
  \ref{thm:HLB}. Further details can be found in \cite[p. 78]{iva}.
\end{p}

\begin{df}
  \label{df:red_gb}
  Let $G \subseteq I$ be a Gröbner basis of $I$. We call $G$ a \textbf{reduced Gröbner
    basis} if for all $g \in G$:
  \begin{enumerate}[label=(\roman*)]
  \item $\mathrm{LC}(g) = 1$.
  \item No monomial of $g$ is in $\langle \mathrm{LT}(G \  \backslash \ \{g\}) \rangle$.
  \end{enumerate}
\end{df}

Most computer algebra systems actually compute reduced Gröbner bases by default.
It can be shown that any ideal has its reduced Gröbner basis and this basis is
unique. When the second condition of Definition~\ref{df:red_gb} holds for a
polynomial $g \in G$, we say that $g$ is fully reduced for $G$. We can obtain the
reduced Gröbner basis as follows. Given $g \in G$, let $g' = \overline{g}^{G \backslash
  \set*{g}}$ and let $G' = \left( G \backslash \{ g \} \right) \cup \set*{g'}$. Observe that
$g'$ is fully reduced for $G'$. If we keep applying this process to all elements
of $G$ until all of them are fully reduced, we end up with the reduced Gröbner
basis.

\begin{df}
  Let $I = \langle \s[m]{f} \rangle \subseteq \Fx{x}$ be an ideal. The $l$-th \textbf{elimination
    ideal} $I_l$ is the ideal of $\F \! \left[ x_{l+1}, \ldots, x_n \right]$ given by
  $I_l = I \cap \F \! \left[ x_{l+1}, \ldots, x_n \right]$.
\end{df}

\begin{thm}[The Elimination Theorem]
  Let $G \subseteq I \subseteq \Fx{x}$ be a Gröbner basis of $I$ so that $x_1 \succeq_{lex} x_2
  \succeq_{lex} \cdots \succeq_{lex} x_n$. Then, for every $0 \le l < n$, the set $G_l = G \cap \F \!
  \left[ x_{l+1}, \ldots, x_n \right]$ is a Gröbner basis of the $l$-th elimination
  ideal $I_l$.
\end{thm}

\begin{p}
  We know that $G_l \subseteq I_l$ by construction, so we only need to show that $\langle
  \LT{I_l} \rangle = \langle \LT{G_l} \rangle$ for a fixed $l$ between 0 and $n$. The inclusion $\langle
  \LT{I_l} \rangle \supseteq \langle \LT{G_l} \rangle$ is evident and to prove $\langle \LT{I_l} \rangle \subseteq \langle \LT{G_l}
  \rangle$, we need to show that $\LT{g} \mid \LT{f}$ for an arbitrary $f \in I_l$ and some
  $g \in G_l$.

  We know that $f$ is also in $I$, so $\LT{g} \mid \LT{f}$ for some $g \in G$ since
  $G$ is a Gröbner basis of $I$. Since $f \in I_l$, $\LT{g}$ must consist only of
  $x_{l+1}, \ldots, x_n$. Now comes the crucial observation: since $x_1 \lex \cdots \lex
  x_n$, any monomial involving any $\s[l]{x}$ is greater than all monomials in
  $\F \! \left[ x_{l+1}, \ldots, x_n \right]$. We see that $g \in G_l$, which proves
  the theorem.
\end{p}

Let us present the power of the Elimination Theorem on the following example.

\begin{e}
  \label{e:gr1}
  Consider a system of equations where $f_1 = f_2 = f_3 = 0$ are polynomials in $\R
  \! \left[ x, y, z \right]$ with
  \begin{align*}
    f_1 &= -16 x^2 - 4 x y^2 + 4 x z, \\
    f_2 &= 4 x^2 z + 2 x y^2 z + z, \\
    f_3 &= x y^2 + 2 y^2 z + 1. \\
  \end{align*}
  If we compute the reduced Gröbner basis with $z \succeq_{lex} y \succeq_{lex}
  x$, we get the following polynomials:
  \begin{align*}
    g_1 &= x + \frac{1}{4} y^2 - \frac{1}{4} z, \\
    g_2 &= y^4 - z^2 - 4, \\
    g_3 &= y^2 z - \frac{1}{9} z^2, \\
    g_4 &= z^3 + \frac{81}{20} z.
  \end{align*}
  We see that the last polynomial involves only the variable $z$ and that its
  only solution in $\R$ is $z = 0$. We can now substitute this solution into
  $g3$ and $g2$ and obtain two solutions, namely $y = \pm \sqrt{2}$. We can
  proceed further and get $x = - \frac{1}{2}$. We see that the reduced Gröbner
  basis allowed us to solve the system.
\end{e}

\begin{df}
  Let $\F_q \! \left[ \s{x} \right]$ be a polynomial ring over the finite field
  $\F_q$ with order $q = p^m$ where $p \in \Prime$ and $m \in \mathbb{N}_{>0}$. The
  \textbf{field equations} of $\F_q$ are the polynomials $x_i^q - x_i$ for every
  $x_i \in \{\s{x}\}$.
\end{df}

\begin{thm}[Finiteness Theorem]
  \sloppy Let $\s[m]{f} \in \Fx{x}$ be polynomials. If we have $\langle \s[m]{f}
  \rangle \cap \F \! \left[ x_i \right] \neq 0$ for all $x_i$, then $V(\langle \s[m]{f} \rangle)
  \subseteq \F^n$ is finite.
\end{thm}

\begin{p}
  See \cite[p. 251]{iva}.
\end{p}

Considering the Finiteness Theorem above, adding the field equations into our
polynomial system ensures that the system will have finitely many solutions.

\begin{thm}[Hilbert's Weak Nullstellensatz]
  Let $\s[m]{f} \in \Fg$ be polynomials. Then the following are equivalent:
  \begin{enumerate}[label=(\roman*)]
  \item There exists an extension field $\E$ of $\F$ and $\mathbf{a} \in \E^n$
    such that for all $f_i$ we have $f_i(\mathbf{a}) = 0$.
  \item $1 \notin \langle \s[m]{f} \rangle$.
  \end{enumerate}
\end{thm}

\begin{p}
  See \cite[p. 281]{gb}.
\end{p}

Observe that whenever 1 belongs to any ideal $I \subseteq \Fg$, we immediately
get $I = \Fg$, as shown in the proof of Proposition \ref{pr:ideal_properties}.

It can be shown that if we have a finite field $\F$ and its extension $\E$, all
elements from $\F$ satisfy all of the field equations of $\F$ and no element in
$\E\ \backslash\ \F$ satisfies any of these equations. Therefore, if we add the
field equations into a polynomial system that consists of polynomials in $\Fg$,
we restrict our solutions to the field $\F$. Let us demonstrate this fact in
combination with the Hilbert's~Weak Nullstellensatz on the following two
examples.

\begin{e}
  \label{e:gr2}
  Consider a system of equations where $f_1 = f_2 = f_3 = 0$ are polynomials in
  GF(2) with
  \begin{align*}
    f_1 &= x + y + z, \\
    f_2 &= x y + x z + y z, \\
    f_3 &= x y z + 1. \\
  \end{align*}
  If we compute the reduced Gröbner basis with $z \succeq_{lex} y \succeq_{lex}
  x$, we get the following polynomials:
  \begin{align*}
    g_1 &= x + y + z, \\
    g_2 &= y^2 + y z + z^2, \\
    g_3 &= z^3 + 1. \\
  \end{align*}
  We see that the only solution to the last polynomial is $z = 1$. When we
  substitute this solution into $g_2$, we get $g_2' = y^2 + y + 1$, which has no
  solution in GF(2), and therefore the initial polynomial system has no solution
  in GF(2) either. We note that $g_2'$ is also irreducible over
  $\mathrm{GF}(2)$. Consider the proof of Proposition \ref{pr:extension_field},
  and let $\alpha$ be a root of $g_2'$; that is, the coset $y + \langle g_2' \rangle$ in $\E =
  \mathrm{GF}(2)[y] / \langle g_2' \rangle$. We get the finite field GF(2)($\alpha$) $\cong$
  GF($2^2$) with the elements $0, 1, \alpha, \alpha + 1$. If $z = 1$, the polynomial $g_2$
  has two solutions in GF($2^2$), namely $y = \alpha$ and $y = \alpha + 1$, since $(\alpha +
  1)^2 = \alpha$. The polynomial $g_1$ has then also two solutions in GF($2^2$),
  namely $x = \alpha$ and $x = \alpha + 1$. All of these solutions also satisfy our
  initial system $f_1, f_2, f_3$. We could also obtain further solutions if we
  set $z = \alpha$.
\end{e}

\begin{e}
  Considering the previous example, if we add the field equations of $\F$ into
  the system, we get the following reduced Gröbner basis:
  \begin{align*}
    g_1 &= 1.
  \end{align*}
  According to the Hilbert's~Weak Nullstellensatz, we can already see that the
  initial polynomial system $f_1, f_2, f_3$ has no solutions in GF(2).
\end{e}
