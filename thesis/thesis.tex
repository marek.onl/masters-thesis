% options:
% thesis=B bachelor's thesis
% thesis=M master's thesis
% czech thesis in Czech language
% english thesis in English language
% hidelinks remove colour boxes around hyperlinks

\documentclass[thesis=M,english]{FITthesis}[2019/12/23]

\usepackage{sagetex}
\usepackage{dirtree}
\usepackage{enumitem}
\usepackage{relsize}
\usepackage{pdfpages}
\usepackage{bm}
\usepackage{varwidth}
\usepackage{tasks}
\usepackage{cprotect}
\usepackage[section]{placeins}
% \usepackage{booktabs}
\usepackage{ctable}

\usepackage{epigraph}
\renewcommand{\epigraphflush}{center}

\usepackage{tablefootnote}
\newcommand{\tf}[1]{\tablefootnote{#1}}

\usepackage{makecell}
\newcommand{\mc}[1]{\makecell{#1}}

\usepackage[binary-units=true]{siunitx}
\usepackage{eurosym}
\DeclareSIUnit{\eur}{\text{\euro}}
\newcommand{\h}{\hour}
\newcommand{\MA}{\mega}
\newcommand{\GA}{\giga}
\newcommand{\B}{\byte}


\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor   = red %Colour of citations
}

\usepackage[section]{algorithm}
\usepackage[noend]{algpseudocode}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage{chngcntr}

\usepackage{mathtools}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}

\usepackage{amsthm}
\usepackage{amssymb}
\numberwithin{equation}{section}
\newcommand{\Fx}[2][n]{\mathbb{F} \! \left[ #2_1, \ldots, #2_#1 \right]}
\newcommand{\F}{\mathbb{F}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Prime}{\mathbb{P}}
\newcommand{\N}{\mathbb{N}_0}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\Fg}{\F[\mb{x}]}
\newcommand{\M}{\mathcal{M}}
\newcommand{\Mx}{\mathcal{M}(\mb{x})}
\newcommand{\s}[2][n]{#2_1, \ldots, #2_{#1}}
\newcommand{\sn}[2][n]{\left( \s[#1]{#2} \right)}
\newcommand{\n}[2][n]{\! \sn[#1]{#2}}
\newcommand{\LT}[1]{\mathrm{LT}(#1)}
\newcommand{\LC}[1]{\mathrm{LC}(#1)}
\newcommand{\LM}[1]{\mathrm{LM}(#1)}
\newcommand{\lcm}[1]{\mathrm{lcm}(#1)}
\newcommand{\Min}[1]{\mathrm{min}(#1)}
\newcommand{\dg}[1]{\mathrm{deg}(#1)}
\newcommand{\sdel}{\ \middle | \ }
\newcommand{\lex}{\succeq_{lex}}

\newtheorem{thm}[subsection]{Theorem}
\newtheorem{pr}[subsection]{Proposition}
\newtheorem{lm}[subsection]{Lemma}

\newtheoremstyle{dfstyle}% % Name
  {}%                      % Space above
  {}%                      % Space below
  {}%                      % Body font
  {}%                      % Indent amount
  {\bfseries}%             % Theorem head font
  {.}%                     % Punctuation after theorem head
  { }%                     % Space after theorem head, ' ', or \newline
  {\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}

\theoremstyle{dfstyle}
\newtheorem{df}[subsection]{Definition}

\theoremstyle{definition}
\newtheorem{e}[subsection]{Example}
\newtheorem{re}[subsection]{Remark}

\newtheoremstyle{break}% name
  {}% Space above, empty = `usual value'
  {}% Space below
  {}% Body font
  {}% Indent amount (empty = no indent, \parindent = para indent)
  {\bfseries}% Thm head font
  {.}% Punctuation after thm head
  {\newline}% Space after thm head: \newline = linebreak
  {}%  Thm head spec

\theoremstyle{break}
\newtheorem{examples}[subsection]{Example}

\newenvironment{es}[1][]{%
\begin{examples}[#1]\leavevmode\vspace{-\baselineskip}%
}{\end{examples}}

\newenvironment{p}[1][\proofname]{%
  \proof[\normalfont{\bfseries #1}]%
}{\endproof}

\newenvironment{ps}[1][\proofname]{%
  \proof[\normalfont{\bfseries #1}]\leavevmode\vspace{-\topskip}%
}{\endproof}

\DeclarePairedDelimiter\set\{\}

% % list of acronyms
\usepackage[acronym,nonumberlist,toc,numberedsection=autolabel]{glossaries}
\makeglossaries

\usepackage[shortcuts]{extdash}

\department{Department of Theoretical Computer Science}

\title{Algebraic Cryptanalysis of Small Scale Variants of AES}

\authorGN{Marek} %author's given name/names

\authorFN{Bielik} %author's surname

\author{Marek Bielik} %author's name without academic degrees

\authorWithDegrees{Marek Bielik} %author's name with academic degrees

\supervisor{Mgr. Martin Jureček}

\acknowledgements{I~thank my supervisor for introducing me to the
realm of algebraic cryptanalysis and my parents for their generosity.}

\abstractEN{This work proposes and demonstrates new advances in algebraic
  cryptanalysis of small scale derivatives of the Advanced Encryption Standard
  (AES). We model AES as a system of polynomial equations over GF(2), which
  involves only the variables of the initial key, and we subsequently attempt to
  solve such a system. We show, for example, that one of the attacks can recover
  the secret key for one round of AES-128 under one minute on a contemporary
  CPU. This attack requires only two known plaintexts and their corresponding
  ciphertexts.}

\abstractCS{Tato práce navrhuje a demonstruje nové postupy v algebraické
  kryptoanalýze zmenšených verzí šifry s~názvem Advanced Encryption Standard
  (AES). Tuto šifru modelujeme jako systém polynomiálních rovnic nad GF(2),
  který zahrnuje pouze proměnné počátečního klíče, a následně se pokoušíme
  takový systém vyřešit. Ukážeme například, že jeden z útoků může na současném
  CPU obnovit tajný klíč pro jedno kolo AES-128 za méně než jednu minutu. Tento
  útok vyžaduje pouze dva známé otevřené texty a jejich odpovídající šifrové
  texty.}

\placeForDeclarationOfAuthenticity{Prague}

\keywordsCS{AES a její zmenšené verze, algebraická kryptoanalýza, Gröbnerovy báze}

\keywordsEN{small scale variants of AES, algebraic cryptanalysis, Gröbner bases}

% select as appropriate, according to the desired license (integer 1-6)
\declarationOfAuthenticityOption{2}

% \website{https://gitlab.com/marek.onl/masters-thesis} %optional thesis URL

\begin{document}

\setsecnumdepth{part}

\include{chapters/introduction}

\setsecnumdepth{all}

\include{chapters/algebra/algebra}

\include{chapters/aes/aes}

\include{chapters/experiments/experiments}

\setsecnumdepth{part}

\bibliographystyle{iso690}
\bibliography{ref}

\setsecnumdepth{all}
\appendix

\chapter{Abbreviations and Symbols}
\printglossaries
\begin{description}
\item[$\mathbb{N}_0 = $] the set of natural numbers including zero
\item[$\mathbb{N}_{>0} = $] the set of natural numbers excluding zero
\item[$\mathbb{Z} = $] the set of integers
\item[$\mathbb{Q} = $] the set of rational numbers (fractions)
\item[$\mathbb{R} = $] the set of real numbers
\item[$\mathbb{C} = $] the set of complex numbers
\item[$\square$] indicates the end of a proof

\item[AES] Advanced Encryption Standard
\item[ANF] Algebraic Normal Form
\item[CNF] Conjunctive Normal Form
\item[SAT] SATISFIABILITY (Boolean satisfiability problem)

\item[e.g.] (Latin \textit{exempli gratia}) for example
\item[i.e.] (Latin \textit{id est}) that is
\item[et al.] (Latin \textit{et alii}) and others
\end{description}


% \chapter{Contents of enclosed CD}

% \begin{figure}
% 	\dirtree{%
% 		.1 readme.txt\DTcomment{the file with CD contents description}.
% 		.1 exe\DTcomment{the directory with executables}.
% 		.1 src\DTcomment{the directory of source codes}.
% 		.2 wbdcm\DTcomment{implementation sources}.
% 		.2 thesis\DTcomment{the directory of \LaTeX{} source codes of the thesis}.
% 		.1 text\DTcomment{the thesis text directory}.
% 		.2 thesis.pdf\DTcomment{the thesis text in PDF format}.
% 		.2 thesis.ps\DTcomment{the thesis text in PS format}.
% 	}
% \end{figure}

\end{document}
